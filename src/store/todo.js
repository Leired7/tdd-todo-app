import { createStore } from 'vuex';

export default {
  state: () => {
    return {
      todos: [],
    };
  },
  mutations: {
    ADD_TODO(state) {
      state.todos.push({
        description: 'Una descripción aleatoria',
        done: false,
      });
    },
  },
};
