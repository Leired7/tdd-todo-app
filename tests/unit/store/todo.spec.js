import todo from '@/store/todo';

describe('The todo store', () => {
  it('uses a function to generate the initial state', () => {
    const newState = todo.state();

    expect(newState).not.toBeUndefined();
  });

  it('stores the todos at the todos key', () => {
    const newState = todo.state();

    expect(newState).toEqual({ todos: [] });
  });
});

describe(', the mutations', () => {
  it('a todo can be added using the ADD_TODO mutation', () => {
    const state = todo.state();

    todo.mutations.ADD_TODO(state, 'Una descripción aleatoria');

    expect(state).toEqual({
      todos: [{ description: 'Una descripción aleatoria', done: false }],
    });
  });
});
