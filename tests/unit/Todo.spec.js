import { shallowMount } from '@vue/test-utils';
import Todo from '@/components/Todo';

describe('The Todo.vue component', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(Todo, {
      propsData: {
        title: 'Un título aleatorio',
      },
    });
  });

  async function addTodo(todoText) {
    wrapper.find('[data-testid= "todo-input"]').setValue(todoText);

    await wrapper.find('[data-testid= "todo-submit"]').trigger('click');
  }

  function elementText(testId) {
    return wrapper.find(`[data-testid= "${testId}"]`).text();
  }

  it('Dispalys the title when passed as a property', () => {
    expect(wrapper.text()).toMatch('Un título aleatorio');
  });

  it('Allows for adding one todo item', async () => {
    await addTodo('Mi primera tarea');

    expect(elementText('todos')).toContain('Mi primera tarea');
  });

  it('When a todo item is added input should be empty', async () => {
    await addTodo('Mi primera tarea');

    expect(wrapper.find('[data-testid="todo-input"]').element.value).toEqual(
      ''
    );
  });

  it('Allows for more than one todo item to be added', async () => {
    await addTodo('Mi primera tarea');

    await addTodo('Mi segunda tarea');

    expect(elementText('todos')).toContain('Mi primera tarea');

    expect(elementText('todos')).toContain('Mi segunda tarea');
  });

  it('Displays the items in the order they are entered', async () => {
    await addTodo('Mi primera tarea');
    await addTodo('Mi segunda tarea');

    expect(elementText('todo-0')).toContain('Mi primera tarea');
    expect(elementText('todo-1')).toContain('Mi segunda tarea');
  });

  it('Items can be marked as done by clicking an element before the item', async () => {
    function itemIsDone(itemId) {
      return (
        wrapper
          .find(`[data-testid="todo-${itemId}"]`)
          .attributes('data-done') === 'true'
      );
    }
    await addTodo('Mi primera tarea');
    await addTodo('Mi segunda tarea');

    expect(itemIsDone(0)).toBe(false);
    await wrapper.find('[data-testid="todo-0-toggle"]').trigger('click');
    expect(itemIsDone(0)).toBe(true);
  });
});
