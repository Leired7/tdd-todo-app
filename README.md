# Obetivo

Este proyecto está basado en el artículo de ["The coding artist"](https://www.thecodingartist.com/blogs/vue-tdd-by-example-create-todo-app/?utm_source=pocket_mylist) en el que crea una aplicación para crear listas haciendo TDD.

También estoy aprovechando este proyecto para aprender a usar Vue y Tailwind.

# Instalar el proyecto

Tras clonar el repositorio y estando en la terminal dentro de la carpeta `tdd-todo-app`.

Ejecutar `npm install`

# Ver el proyecto

Estando en la terminal dentro de la carpeta `tdd-todo-app`.

Ejecutar `npm run serve`

# Ver los test unitarios

estando en la terminal dentro de la carpeta `tdd-todo-app`.

Ejecutar `npm run tdd`

### Lint

```
npm run lint
```

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```
